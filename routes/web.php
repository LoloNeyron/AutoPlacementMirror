<?php

use App\Http\Controllers\SeatingPlansController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// HomeController routes
Route::get('/', "HomeController@index");

//SeatingPlansController routes
Route::get('/seating-plans', "SeatingPlansController@index")->name('seating-plans');
Route::get('/seating-plans/create', "SeatingPlansController@create")->name('seating-plans.create');
Route::post('/seating-plans/create', "SeatingPlansController@store")->name('seating-plans.store');
Route::get('/seating-plans/{id}/edit', "SeatingPlansController@edit")->name('seating-plans.edit');
Route::post('/seating-plans/{id}/edit', "SeatingPlansController@update")->name('seating-plans.update');
Route::get('/seating-plans/delete/{id}', "SeatingPlansController@delete")->name('seating-plans.delete');

// StorageFileReaderController routes
Route::get('storage/{filename}', "StorageFileReaderController@read");

// AutoplacementController routes
Route::get('/placement', "AutoplacementController@index")->name('autoplacement');
Route::get('/placement/{id}', "AutoplacementController@show")->name('autoplacement.show');
Route::get('/placement/json/seaters-by-group/{id}', "AutoplacementController@getSeatersByGroupId");

//button radom
Route::post('/placement/random/{id}', "AutoplacementController@placement")->name('autoplacement.random');

//button Email
Route::get('/placement/email/{id}', "AutoplacementController@sendEmail")->name('autoplacement.email');
// Route::get('/placement/{id}/testemail', function() {
//     return new App\Mail\OrderShipped('cou');
// });

// LoginController routes
Route::get('/logout', 'Auth\LoginController@logout');
Auth::routes();