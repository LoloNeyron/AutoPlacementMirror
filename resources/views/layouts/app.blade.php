<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @section('title')
            Assistant de plan de salle automatique et aléatoire - SeatingCharts
        @endsection
        @yield('title')
    </title>
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-dark rgba-blue-strong fixed-top scrolling-navbar">
        <a class="navbar-brand" href="{{ url('/') }}">SeatingCharts <span class="badge badge-secondary">Alpha</span></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav try">

                @if (Auth::guest())
                    <li class="nav-item">
                        <a href="{{route('login')}}" class="nav-link" data-toggle="modal"
                           data-target="#elegantModalForm2" style="position: relative;top: 2px;">Connexion</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('register')}}" class="btn btn-primary nav-link" data-toggle="modal"
                           data-target="#elegantModalForm" style="height: 33px;line-height: 11px">Inscription</a></li>
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('autoplacement')}}">Placements</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('seating-plans')}}">Plans de salle</a>
                    </li>
                    <a href="{{ url('/logout') }}" class="nav-link">Déconnexion</a>
                @endif
                    <!-- <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Dropdown link
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>-->
            </ul>
        </div>
        <div class="clearfix"></div>
        @foreach($errors->all() as $key => $error)
            <div class="alert alert-danger  alert-dismissible fade show" role="alert"
                 style="position: absolute;bottom: -67px;width: 100%;left: 0;">
                {{ $error }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endforeach
    </nav>
</header>


<main class="">
    @yield('content')
</main>

<footer class="app-footer marg">
    <div class="container">
        <div class="row">
            <div class="col-4">
                <p>Apparatu conperto editos tricensimum accipiens imperii siquid pro inter Gerontium theatralis siquid
                    editos accipiens conperto.</p>
            </div>
            <div class="col-4">
                <p>Apparatu conperto editos tricensimum accipiens imperii siquid pro inter Gerontium theatralis siquid
                    editos accipiens conperto.</p>
                <p><a href="mailto:contact@simplon-roanne.com">contact@simplon-roanne.com</a></p>
            </div>
            <div class="col-4">
                <p>Apparatu conperto editos tricensimum accipiens imperii siquid pro inter Gerontium theatralis siquid
                    editos accipiens conperto.</p>
            </div>
        </div>
    </div>

</footer>

@include('partials.auth-modals')

<!-- Scripts -->
@section('scripts')
    <script src="{{ asset('js/app.js') }}" defer></script>
@endsection
@yield('scripts')
</body>
</html>
