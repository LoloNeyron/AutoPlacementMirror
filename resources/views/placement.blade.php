<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Génération du placement</title>
</head>
<body>

    <canvas id="c" width="500" height="500" style="border:1px solid #ccc"></canvas>
    <br>
    <button onclick="test();">TEST !</button>


    <script>

        function test(){
            console.log('run');
            $('#c').remove();
            var c = document.createElement('canvas');
            c.id = "c";
            $(c).prepend('body');
        }

    </script>
</body>
</html>