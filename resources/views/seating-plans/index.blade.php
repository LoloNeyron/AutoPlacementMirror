@extends('layouts.app')

@section('content')
    <div class="container mb-50 mt-100">
        <h1 class="display-4">Plans de salle</h1>
        <p class="lead">Gérer la liste de vos plans de salle et les élèves associés</p>

        <div class="row justify-content-center">
            <div class="col-md-12">
                <a href="{{ route('seating-plans.create') }}" class="btn btn-outline-primary">➕ Créer un plan de
                    salle</a><br><br>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Prévisualisation</th>
                        <th scope="col">Titre</th>
                        <th scope="col">Mis à jour le</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($seatingPlans as $seatingPlan)
                        <tr>
                            <td><img src="{{ url('storage/'.$seatingPlan->thumbnail) }}" style="    background: #fff;">
                            </td>
                            <td scope="row">{{ $seatingPlan->title }}</td>
                            <td>{{ $seatingPlan->created_at->format('d/m/Y') }}</td>
                            <td style="font-size: 40px;">
                                <a href="{{ route('seating-plans.delete', ['id' => $seatingPlan->id]) }}"
                                   onclick="return confirm('Etes vous sur ?')"
                                    data-toggle="tooltip"
                                    title="Supprimer">❌</a>
                                <a href="{{ route('seating-plans.edit', ['id' => $seatingPlan->id]) }}"
                                   data-toggle="tooltip"
                                   title="Modifier">📝</a>
                                <a href="{{ route('autoplacement.show', ['id' => $seatingPlan->id]) }}"
                                   data-toggle="tooltip"
                                   title="Générer un placement automatique à partir de ce plan de salle">🔀</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/seating-plans.js') }}" defer></script>
@endsection