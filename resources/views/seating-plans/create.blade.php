@extends('layouts.app')

@section('content')
    <div class="jumbotron jumbotron-fluid" id="createSeatingPlanVue">
        <form class="container"  method="post" v-on:submit.prevent="sendForm">
            @csrf
            <h1 class="display-4">Créer un plan de salle</h1>
            <div class="input-group mt-1 mb-3">
                <div class="input-group-prepend">
                    <label for="seatingPlanTitle" class="input-group-text">Titre</label>
                </div>
                <input type="text" class="form-control" name="title" id="seatingPlanTitle" value="{{ $seatingPlan->title }}" required>
            </div>
            <p class="lead">Dessinez votre salle à l'aide de tables et de places assises</p>
            <hr class="display-4">
            <auto-placement ref="editor" json="{{ $seatingPlan->json }}"></auto-placement>

            <h1 class="display-4 mt-5" >Designez des groupes d'élèves</h1>
            <p class="lead">Ajoutez ou importez vos élèves à partir d'un fichier</p>

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
                Ajouts d'éleves par ficher Csv/Xlxs
            </button>
            <hr class="display-4">
            <seaters-manager :seaters-by-group-init="{{ json_encode($seatersByGroup)}}" :imported="importedSeaters"></seaters-manager>
            <hr class="display-4">
            <button class="btn btn-primary btn-lg float-right">Enregistrer</button>
        </form>

        <!-- Modal -->
        <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <seaters-importer v-on:seaters-imports-results-changed="setSeaters"></seaters-importer>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script src="{{ asset('js/seating-plan-create.js') }}" defer></script>
@endsection
