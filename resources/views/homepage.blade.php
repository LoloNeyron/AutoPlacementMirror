@extends('layouts.app')

@section('content')
    <!--Carousel-->
    <section>
        <div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
            <!--Slides-->
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <div class="view">
                            <img class="d-block w-100 h-100 img-fluid" src="assets/startup.jpg" alt="First slide">
                            <div class="mask rgba-black-light"></div>
                        </div>
                        <div class="carousel-caption">
                            {{--<center><h1 class="title align-center">Placement automatique sur disposition personnalisée</h1></center>--}}
                            <div class="animated fadeInDown">
                                <h3 class="h3-responsive texte-carousel">Placement automatique sur plan de salle personnalisé</h3>
                                <p>Dessinez votre plan de salle et générez des placements aléatoires pour vos salles de classes ou de formation. Planifiez l'envoi automatique à vos élèves, étudiants ou apprenants.</p>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <!--Mask color-->
                        <div class="view">
                            <img class="d-block w-100 img-fluid" src="assets/carousel2.jpg" alt="Second slide">
                            <div class="mask rgba-black-strong"></div>
                        </div>
                        <div class="carousel-caption">
                            {{--<center><h1 class="title align-center">Placement automatique sur disposition personnalisée</h1></center>--}}
                            <div class="animated fadeInDown">
                                <h3 class="h3-responsive texte-carousel">Placement aléatoire selon vos règles</h3>
                                <p>Créez des règles personnalisées par individu ou par groupe. Générez un placement aléatoire adapté à vos besoins.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/.Slides-->
                <!--Controls-->
                <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Precedent</span>
                </a>
                <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Suivant</span>
                </a>
                <!--/.Controls-->
            </div>
        </div>
    </section>
    <!--/ End Carousel-->
    <!-- Section creer dessiner -->
    <section id="features" class="section1">
        <div class="container">
            <div class="row texte2 justify-content-center marg">
                <!-- 3 colonnes pour les paragraphes, icones récupéré sur flaticon.com -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="icon"><i class="fa fa-drafting-compass homepage-icon-effect"></i></div>
                    <h3>1. Dessinez votre plan de salle</h3>
                    <p class="text-justify">Nous avons développé un outil qui facilite la création des plans de salle. Gagnez du temps précieux !</p>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="icon"><i class="fa fa-random homepage-icon-effect"></i></div>
                    <h3>2. Placez les participants</h3>
                    <p class="text-justify">Créez des règles de placement et générez des placements aléatoires. Cette méthode prouvée optimise l'apprentissage d'une classe.</p>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="icon"><i class="fa fa-calendar-alt homepage-icon-effect"></i></div>
                    <h3>3. Planifiez l'envoi</h3>
                    <p class="text-justify">Quotidien, hebdomadaire, ou manuel : nous enverrons votre plan de salle par email, sms, discord ou notification.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- End section creer dessiner -->
    <!-- Parallax -->
    <section>
        <div class="parallax img-fluid">
            <div class="container">
                <div class="row">
                    <div class="text-parallax">
                        <h1 class="h-parallax intro">
                            <b>R</b><b>e</b><b>j</b><b>o</b><b>i</b><b>g</b><b>n</b><b>e</b><b>z</b><b>-</b><b>n</b><b>o</b><b>u</b><b>s</b>
                        </h1>
                        <p class="col-12 col-md-6 col-lg-6 col-xl-6 offset-md-3 offset-lg-3">Inscrivez-vous dès maintenant pour créer la disposition de vos salles et enregistrer les participants.</p>
                        <div class="test2 col-6 col-md-4 col-lg-2 col-xl-2">
                            <button type="submit" class="btn hvr-effect btn-block z-depth-1">S'inscrire</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End parallax -->
    <!-- Section contact -->
    <section class="marg">
        <div class="container">
            <div class="row">
                <div class="col-12 offset-lg-1 col-lg-7 col-md-7 col-sm-12">
                    <h1><b>Des questions ou commentaires ?</b></h1>
                    <h5>Contactez-nous, nous serons là pour vous répondre</h5>
                </div>
                <div class="col-6 offset-2 col-lg-2 offset-lg-1 col-md-4 offset-md-1 col-sm-2">
                    <button type="submit" class="btn hvr-effect marg-bouton-contact btn-block z-depth-1">Contact</button>
                </div>
            </div>
        </div>
    </section>
    <!-- End section contact -->
    <!-- Effect image -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6">
                    <header class="codrops-header"></header>
                    <div class="grid">
                        <figure class="effect-layla">
                            <img class="img-fluid" src="assets/effect.jpg" alt="img04"/>
                            <figcaption>
                                <h2>Ma<span>place</span></h2>
                                <p><b>Où vais-je me trouver aujourd'hui ?</b></p>
                                {{--<a href="#">View more</a>--}}
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-6 text-section-effect">
                    <h1 class="title-effect">Quos Olybrium negotio eius</h1>
                    <p class="text-effect">
                        Haec igitur Epicuri non probo, inquam. De cetero vellem equidem aut ipse doctrinis fuisset instructior est enim,
                        quod tibi ita videri necesse est, non satis politus iis artibus, quas qui tenent, eruditi appellantur aut ne
                        deterruisset alios a studiis. quamquam te quidem video minime esse deterritum.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- End effect image -->
@endsection