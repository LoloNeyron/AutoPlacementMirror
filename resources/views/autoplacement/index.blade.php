@extends('layouts.app')

@section('content')
    {{-- liste de tous les plans de tables--}}

    <div class="container mt-100 mb-50">
        <h1 class="display-4">Selection du plan de table</h1>
        <p class="lead">Cliquez sur le plan de salle pour lequel vous souhaitez génerer un placement automatique.</p>


        <div class="row text-center">
                @foreach($seatingPlans as $seatingPlan)
                <div class="col-md-3">
                    <a href="placement/{{$seatingPlan->id}}">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="{{ url('storage/'.$seatingPlan->thumbnail) }}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">{{$seatingPlan->title}}</h5>
                            <p class="card-text">
                                Créé le {{$seatingPlan->created_at->format('d/m/Y')}}
                                @if($seatingPlan->created_at->format('d/m/Y') !== $seatingPlan->updated_at->format('d/m/Y'))
                                    <br>
                                    Mis à jour  le {{$seatingPlan->updated_at->format('d/m/Y')}}
                                @endif
                            </p>
                            <a href="#" class="btn btn-primary">Sélectionner</a>
                        </div>
                    </div></a>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection

@section('scripts')

@endsection
