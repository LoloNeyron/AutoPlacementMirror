
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page.
 */

import SeaterGroupSelector from './components/SeaterGroupSelector.vue'

new Vue({
    el: '#showPlacement',
    components : {
        SeaterGroupSelector
    },
    methods: {
        generateIfHasGroup(e) {
            if(!$('[name="group_id"]').val()) {
                alert("Vous devez d'abord sélectionner un groupe");
                e.preventDefault()
            }
        }
    }
});