export default {
    'Roboto Condensed': 'sans-serif',
    'Lato': 'sans-serif',
    'Open Sans': 'sans-serif',
    'Slabo 27px': 'serif',
    'Open Sans Condensed': 'sans-serif',
    'Patrick Hand': 'cursive',
    'Indie Flower': 'cursive',
    'Josefin Sans': 'sans-serif',
    'VT323': 'sans-serif'
}