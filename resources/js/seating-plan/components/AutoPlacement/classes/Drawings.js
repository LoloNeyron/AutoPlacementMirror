import {FabricShape} from './Shapes'

class DrawableShape extends FabricShape {
    constructor(pointer) {
        super();
        this.origX = pointer.x;
        this.origY = pointer.y;

        this.options = {
            ...this.getOptions(),
            ...{
                left: pointer.x,
                top: pointer.y,
            }
        };

        this.setFabricObject();
    }

    getOptions() {
        return {
            strokeWidth: 1,
            stroke: '#132433',
            selectable: false,
            fill: this.getShapeColor(),
            originX: 0,
            originY: 0,

        };
    }

    getShapeColor() {
        return '#eee';
    }
    updateDimensions(pointer) {
        let params;

        if (this.origX < pointer.x) {
            params = {
                width: -(this.origX - pointer.x),
                height: -(this.origY - pointer.y),
                left: this.origX,
                top: this.origY
            };

            if (params.height < 0) {
                params.top = pointer.y;
            }
        }
        else {
            params = {
                width: this.origX - pointer.x,
                height: this.origY - pointer.y,
                left: pointer.x,
                top: pointer.y
            };

            if (params.height < 0) {
                params.top = this.origY;
            }
        }

        if (params.height < 0) {
            params.height = Math.abs(params.height);
        }

        this.fabricObject.set(params);

        return params;
    }
}

class DrawableShapeWithText extends DrawableShape {
    setText(text) {
        this.setTextSettings({text: text.toString()});
    }
}

class TableDrawing extends DrawableShape {
    _getFabricObjectInstance() {
        return new fabric.Rect(this.options);
    }
}

class SeatDrawing extends DrawableShapeWithText {
    _getFabricObjectInstance() {
        let circle = new fabric.Circle(this.getOptions());

        let text = new fabric.Textbox('0', {
            originX: 'center',
            originY: 'center',
            fontSize: 30,
            fill: '#fff'
        });

        return new fabric.Group([circle, text], this.options);
    }

    getShapeColor() {
        return 'rgba(63, 81, 181, 0.9)';
    }

    getOptions() {
        return {
            ...super.getOptions(),
            ...{
                originX: 'center',
                originY: 'center',
                radius: 1,
            }
        };
    }

    updateDimensions(pointer) {
        let xRadius, yRadius;

        if(this.origX > pointer.x) {
            xRadius = this.origX - pointer.x;
        }
        else {
            xRadius = pointer.x - this.origX;
        }
        if(this.origY > pointer.y) {
            yRadius = this.origY - pointer.y;
        }
        else {
            yRadius = pointer.y - this.origY;
        }

        const radius = Math.max(xRadius, yRadius);

        this.fabricObject.set({
            left: this.origX,
            top: this.origY,
            height:radius*2,
            width:radius*2,
        });

        this.fabricObject.getObjects()[0].set({
            radius: Math.abs(radius),
        });
    }

    setTextSettings(settings) {
        this.fabricObject.getObjects()[1].set(settings);
    }
}

class TextDrawing extends DrawableShapeWithText {
    _getFabricObjectInstance() {
        return new fabric.IText("Texte à éditer", this.options)
    }

    getShapeColor() {
        return '#000';
    }

    setTextSettings(settings) {
        this.fabricObject.set(settings);
    }
}


export {DrawableShapeWithText, TableDrawing, SeatDrawing, TextDrawing};