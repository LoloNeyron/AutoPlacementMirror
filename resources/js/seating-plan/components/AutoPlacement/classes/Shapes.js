class FabricShape {
    constructor(fabricObject = null) {
        if(fabricObject) {
            this.setFabricObject(fabricObject);
        }
    }

    setFabricObject(newObject = null) {
        this.fabricObject = newObject ? newObject : this._getFabricObjectInstance();
        // Cross reference needed
        this.fabricObject.shape = this;
    }

    clone(callback) {
        let shapeClone = Object.assign(Object.create(Object.getPrototypeOf(this)), this);

        shapeClone.fabricObject.clone(objectClone => {
            shapeClone.setFabricObject(objectClone);
            callback(shapeClone);
        });
    }

    set(settings) {
        this.fabricObject.set(settings);
    }

    addToCanvas(canvas) {
        if (this.fabricObject.type === 'activeSelection') {
            // active selection needs a reference to the canvas.
            this.fabricObject.canvas = canvas;
            this.fabricObject.forEachObject((obj) => {
                canvas.add(obj);
            });
            // this should solve the unselectability
            this.fabricObject.setCoords();
        } else {
            canvas.add(this.fabricObject);
        }
    }
}

class SeatShape extends FabricShape {
    setText(text) {
        this.setTextSettings({text: text.toString()});
    }
    setTextSettings(settings) {
        this.fabricObject.getObjects()[1].set(settings);
    }
}

class ShapeSelection extends FabricShape {
    setFabricObject(fabricObject) {
        super.setFabricObject(fabricObject);

        fabricObject.getObjects().forEach(object => {
            new FabricShape(object);
        })
    }
}

export {FabricShape, SeatShape, ShapeSelection};