import Vue from 'vue'
import Vuex from 'vuex'

import {SELECT_NEW_TOOL, CANVAS_SET, CANVAS_ADD_EVENTS, CANVAS_SAVE} from './mutation-types'
import {TableDrawing} from "../components/AutoPlacement/classes/Drawings";

Vue.use(Vuex);


let canvasEventsToAdd = {};
const addCanvasEventListeners = (object, events) => {
    for(let [eventName, callbacks] of Object.entries(events)) {
        if(typeof callbacks === "function") {
            callbacks = [callbacks]
        }
        callbacks.forEach(callback => {
            object.on(eventName, callback)
        });
    }
};

export default new Vuex.Store({
    state: {
        canvas: null,
        json: null,
        undos: [],
        redos: [],
        selectedTool: 'table',
    },
    getters: {
    },
    mutations: {
        [CANVAS_SET](state, canvas) {
            state.canvas = canvas;
        },
        [CANVAS_ADD_EVENTS](state, events) {
            if(!state.canvas) {
                for(let [eventName, callback] of Object.entries(events)) {
                    if(!Array.isArray(canvasEventsToAdd[eventName])) {
                        canvasEventsToAdd[eventName] = [];
                    }
                    canvasEventsToAdd[eventName].push(callback);
                }
            }
            else {
                addCanvasEventListeners(state.canvas, events);
            }
        },
        [CANVAS_SAVE](state) {
            // clear the redo stack
            state.redos = [];

            // initial call won't have a state
            if (state.json) {
                state.undos.push(state.json);
            }
            state.json = state.canvas.getState();
        },
        [SELECT_NEW_TOOL](state, {tool, shapeClass}) {
            state.selectedTool = tool;

            // TODO: replace with vue template logic
            let selectedInputSelector = '[value="' + tool + '"]';
            document.querySelector("input" + selectedInputSelector).parentElement.classList.add('active');
            document.querySelectorAll("input[name='forme']:not(" + selectedInputSelector + ")").forEach(function (input) {
                input.parentElement.classList.remove('focus', 'active');
            });

            let isSelectionMode = tool === 'selection';
            if (!isSelectionMode) {
                state.canvas.setActiveShapeClass(shapeClass);
            }
            state.canvas.toggleSelectionMode(isSelectionMode);
        },
    },
    actions: {
        initCanvas({commit, state}, canvas) {
            commit(CANVAS_SET, canvas);
            addCanvasEventListeners(canvas, canvasEventsToAdd);
            commit(SELECT_NEW_TOOL, {
                tool: state.selectedTool,
                shapeClass: TableDrawing
            });
        }
    }
})