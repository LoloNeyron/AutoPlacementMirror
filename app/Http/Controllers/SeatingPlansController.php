<?php

namespace App\Http\Controllers;

use App\Seater;
use App\SeaterGroup;
use App\SeatingPlan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SeatingPlansController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seatingPlans = SeatingPlan::where('user_id', auth()->id())->get();

        return view('seating-plans.index', compact('seatingPlans'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $seatingPlan = new SeatingPlan();

        $seatersByGroup = [];

        return view('seating-plans.create', compact('seatingPlan', 'seatersByGroup'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        // dd(\request()->all());

        $seatingPlan = new SeatingPlan(request()->only(['title', 'json', 'svg']));
        $seatingPlan->user_id = auth()->id();
        $seatingPlan->save();
        $this->saveThumbnail($seatingPlan);

        if(request()->has('seaters_groups')) {
            foreach(request()->get('seaters_groups') as $groupData) {
                $seaterGroup = $this->saveGroup(new SeaterGroup(), $seatingPlan, $groupData);

                if(isset($groupData["subgroups"])) {
                    foreach($groupData["subgroups"] as $subGroupData) {
                        $this->saveGroup(new SeaterGroup(), $seatingPlan, $subGroupData, $seaterGroup->id);
                    }
                }
            }
        }

        return redirect()->to('/seating-plans');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seatingPlan = SeatingPlan::find($id);

        $seatersByGroup = SeaterGroup::with('seaters', 'subgroups', 'subgroups.seaters')
            ->where('seating_plan_id', $id)
            ->whereNull('parent_id')
            ->get()
            ->toArray();
        
        return view('seating-plans.create', compact('seatingPlan', 'seatersByGroup'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {

        $seatingPlan = SeatingPlan::find($id);
        $seatingPlan->update($request->only(['title', 'json', 'svg']));
        $this->saveThumbnail($seatingPlan);

        if(request()->has('seaters_groups')) {
            foreach(request()->get('seaters_groups') as $groupData) {
                if($groupData["id"]) {
                    $seaterGroup = SeaterGroup::find($groupData["id"]);
                    SeaterGroup::where('parent_id', $seaterGroup->id)->delete();
                    Seater::where('seater_group_id', $seaterGroup->id)->delete();
                }
                else {
                    $seaterGroup = new SeaterGroup();
                }

                $seaterGroup = $this->saveGroup($seaterGroup, $seatingPlan, $groupData);

                if(isset($groupData["subgroups"])) {
                    foreach($groupData["subgroups"] as $subGroupData) {
                        $this->saveGroup(new SeaterGroup(), $seatingPlan, $subGroupData, $seaterGroup->id);
                    }
                }
            }
        }

        //TODO faire une message flash pour annoncé que l'update a réussi.

        return redirect('/seating-plans');
    }

     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $seatingPlan = SeatingPlan::find($id)->delete();

        //Todo: ajouter message flash de confirmation

        return redirect('/seating-plans');
    }

    private function saveGroup(SeaterGroup $seaterGroup, SeatingPlan $seatingPlan, $data, $parent_id = null)
    {
        $seaterGroup->user_id = auth()->id();
        $seaterGroup->name = $data["name"];
        $seaterGroup->seating_plan_id = $seatingPlan->id;
        if($parent_id) {
            $seaterGroup->parent_id = $parent_id;
        }
        $seaterGroup->save();

        if(isset($data["seaters"])) {
            foreach($data["seaters"] as $seatData) {
                $seater = new Seater($seatData);
                $seater->seater_group_id = $seaterGroup->id;
                $seater->save();
            }
        }

        return $seaterGroup;
    }

    private function saveThumbnail(SeatingPlan $seatingPlan)
    {
        $filename = $seatingPlan->id.'-thumbnail.png';
        $thumbnailContent = str_replace('data:image/png;base64,', '', request('thumbnail'));
        $thumbnailContent = str_replace(' ', '+', $thumbnailContent);
        Storage::put($filename, base64_decode($thumbnailContent));
        $seatingPlan->thumbnail = $filename;
        $seatingPlan->save();
    }
}
