<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Relation de type 1:n
     * 
     * Get the seaterGroup from user.
     */
    public function seaterGroup()
    {
        return $this->hasMany('App\SeaterGroup');
    }

    /**
     * Relation de type 1:n
     * 
     * Get the seatingPlan from user.
     */
    public function seatingPlan()
    {
        return $this->hasMany('App\SeatingPlan');
    }

    /**
     * Relation de type 1:n
     * 
     * Get the seatingPlacement from user.
     */
    public function seatingPlacement()
    {
        return $this->hasMany('App\SeatingPlacement');
    }
}
